#include "Zone.h"

Zone::Zone(sql::Connection* conn, int zoneId, std::string name, std::string description)
{
	_connection = conn;
	_zoneId = zoneId;
	_name = name;
	_description = description;
}

Zone::Zone(int zoneId, std::string name, std::string description)
{
	//_connection = nullptr;
	_zoneId = zoneId;
	_name = name;
	_description = description;
}

std::vector<Map> Zone::getAssignedMaps()
{
	return std::vector<Map>();
}

std::string Zone::show() const
{
	return "Zone -> id: " + std::to_string(_zoneId) + "; name: " + _name + "; description: " + _description;
}