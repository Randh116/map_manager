#include <stdlib.h>
#include <iostream>

#include "mysql_connection.h"
#include <cppconn/driver.h>
#include <cppconn/exception.h>
#include <cppconn/prepared_statement.h>

#include "Database.h"

//const std::string server = "tcp://127.0.0.1:3306";
//const std::string username = "root";
//const std::string password = "P4r0owK@";
//const std::string schema = "mapdb"; 


Database::Database(std::string server, std::string username, std::string password, std::string schema)
{
    //_connection = nullptr;

    _server = server;
    _username = username;
    _password = password;
    _schema = schema;
}

sql::Connection* Database::connect() {
    sql::Driver* driver;
    sql::Connection* conn;

    try
    {
        driver = get_driver_instance();
        conn = driver->connect(_server, _username, _password);
    }
    catch (sql::SQLException e)
    {
        std::cout << "Could not connect to server. Error message: " << e.what() << std::endl;
        system("pause");
        exit(1);
    }

    conn->setSchema(_schema);

    _connection = conn;

    return conn;
}

void Database::displayMaps() const
{
    std::string mapList = "";
    for (auto&& map : _maps) {
        mapList += map.show() + "\n";
    }

    std::cout << mapList;
}

void Database::displayTracks() const
{
    std::string trackList = "";
    for (auto&& track : _tracks) {
        trackList += track.show() + "\n";
    }

    std::cout << trackList;
}

void Database::displayTrackMarks() const
{
    std::string trackMarkList = "";
    for (auto&& trackMark : _trackMarks) {
        trackMarkList += trackMark.show() + "\n";
    }

    std::cout << trackMarkList;
}

void Database::displayZones() const
{
    std::string zoneList = "";
    for (auto&& zone : _zones) {
        zoneList += zone.show() + "\n";
    }

    std::cout << zoneList;
}

bool Database::execAdd(sql::PreparedStatement* pstmt) 
{
    bool isAdded;
    
    try
    {
        pstmt->execute();
        isAdded = true;
    }
    catch (sql::SQLException e)
    {
        std::cout << "Error while adding new record. Error message: " << e.what() << std::endl;
        isAdded = false;
    }

    delete pstmt;

    return isAdded;
}

bool Database::execDelete(sql::PreparedStatement* pstmt)
{
    bool isDelete;

    try
    {
        pstmt->execute();
        isDelete = true;
    }
    catch (sql::SQLException e)
    {
        std::cout << "Error while deleting a record. Error message: " << e.what() << std::endl;
        isDelete = false;
    }

    delete pstmt;

    return isDelete;
}

bool Database::addToDb(Zone* zone)
{
    sql::PreparedStatement* pstmt = _connection->prepareStatement("insert into zone(ZoneId, Name, Description) values (?,?,?)");
    pstmt->setInt(1, zone->getZoneId());
    pstmt->setString(2, zone->getName());
    pstmt->setString(3, zone->getDescription());

    bool isAdded = execAdd(pstmt);

    return isAdded;
}

bool Database::addToDb(Map* map)
{
    sql::PreparedStatement* pstmt = _connection->prepareStatement("insert into map(MapId, ZoneId, Name, XRange, YRange) values (?,?,?,?,?)");
    pstmt->setInt(1, map->getMapId());
    pstmt->setInt(2, map->getZoneId());
    pstmt->setString(3, map->getName());
    pstmt->setDouble(4, map->getXRange());
    pstmt->setDouble(5, map->getYRange());

    bool isAdded = execAdd(pstmt);

    return isAdded;
}

bool Database::addToDb(Track* track)
{
    sql::PreparedStatement* pstmt = _connection->prepareStatement("insert into track(TrackId, MapId, TrackName) values (?,?,?)");
    pstmt->setInt(1, track->getTrackId());
    pstmt->setInt(2, track->getMapId());
    pstmt->setString(3, track->getName());

    bool isAdded = execAdd(pstmt);

    return isAdded;
}

bool Database::addToDb(TrackMark* trackMark)
{
    sql::PreparedStatement* pstmt = _connection->prepareStatement("insert into trackmark(TrackMarkId, TrackId, MarkName, MarkDescription, IsActive) values (?,?,?,?,?)");
    pstmt->setInt(1, trackMark->getTrackMarkId());
    pstmt->setInt(2, trackMark->getTrackId());
    pstmt->setString(3, trackMark->getName());
    pstmt->setString(4, trackMark->getDescription());
    pstmt->setBoolean(5, trackMark->getActive());

    bool isAdded = execAdd(pstmt);

    return isAdded;
}

bool Database::deleteFromDb(Zone* zone)
{
    sql::PreparedStatement* pstmt = _connection->prepareStatement("delete * from zone where ZoneId = (?)");
    pstmt->setInt(1, zone->getZoneId());

    bool isDeleted = execDelete(pstmt);

    return isDeleted;
}

bool Database::deleteFromDb(Map* map)
{
    sql::PreparedStatement* pstmt = _connection->prepareStatement("delete * from map where MapId = (?)");
    pstmt->setInt(1, map->getMapId());

    bool isDeleted = execDelete(pstmt);

    return isDeleted;
}

bool Database::deleteFromDb(Track* track)
{
    sql::PreparedStatement* pstmt = _connection->prepareStatement("delete * from track where TrackId = (?)");
    pstmt->setInt(1, track->getTrackId());

    bool isDeleted = execDelete(pstmt);

    return isDeleted;
}

bool Database::deleteFromDb(TrackMark* trackMark)
{
    sql::PreparedStatement* pstmt = _connection->prepareStatement("delete * from trackmark where TrackMarkId = (?)");
    pstmt->setInt(1, trackMark->getTrackMarkId());

    bool isDeleted = execDelete(pstmt);

    return isDeleted;
}

Zone Database::getZoneById(int id)
{
    std::vector<Zone> tmpVector;
    sql::PreparedStatement* pstmt = _connection->prepareStatement("select * from zone where ZoneId = (?)");
    pstmt->setInt(1, id);

    sql::ResultSet* result = pstmt->executeQuery();
    while (result->next()) {
        tmpVector.push_back(Zone(result->getInt("ZoneId"), result->getString("Name"), result->getString("Description")));
    }

    delete pstmt;

    return tmpVector.back();
}

Map Database::getMapById(int id)
{
    std::vector<Map> tmpVector;
    sql::PreparedStatement* pstmt = _connection->prepareStatement("select * from map where MapId = (?)");
    pstmt->setInt(1, id);

    sql::ResultSet* result = pstmt->executeQuery();
    while (result->next()) {
        tmpVector.push_back(Map(result->getInt("MapId"), result->getInt("ZoneId"), result->getString("Name"), result->getDouble("XRange"), result->getDouble("YRange")));
    }

    delete pstmt;

    return tmpVector.back();
}

Track Database::getTrackById(int id)
{
    std::vector<Track> tmpVector;
    sql::PreparedStatement* pstmt = _connection->prepareStatement("select TrackId, MapId, TrackName from track where TrackId = (?)");
    pstmt->setInt(1, id);

    sql::ResultSet* result = pstmt->executeQuery();
    while (result->next()) {
        tmpVector.push_back(Track(result->getInt("TrackId"), result->getInt("MapId"), result->getString("TrackName")));
    }

    delete pstmt;

    return tmpVector.back();
}

TrackMark Database::getTrackMarkById(int id)
{
    std::vector<TrackMark> tmpVector;
    sql::PreparedStatement* pstmt = _connection->prepareStatement("select * from trackmark where TrackMarkId = (?)");
    pstmt->setInt(1, id);

    sql::ResultSet* result = pstmt->executeQuery();
    while (result->next()) {
        tmpVector.push_back(TrackMark(result->getInt("TrackMarkId"), result->getInt("TrackId"), result->getString("MarkName"), result->getString("MarkDescription"), result->getBoolean("IsActive")));
    }

    delete pstmt;

    return tmpVector.back();
}

Zone Database::getZoneByName(std::string name)
{
    std::vector<Zone> tmpVector;
    sql::PreparedStatement* pstmt = _connection->prepareStatement("select * from zone where Name = (?)");
    pstmt->setString(1, name);

    sql::ResultSet* result = pstmt->executeQuery();
    while (result->next()) {
        tmpVector.push_back(Zone(result->getInt("ZoneId"), result->getString("Name"), result->getString("Description")));
    }

    delete pstmt;

    return tmpVector.back();
}

Map Database::getMapByName(std::string name)
{
    std::vector<Map> tmpVector;
    sql::PreparedStatement* pstmt = _connection->prepareStatement("select * from map where Name = (?)");
    pstmt->setString(1, name);

    sql::ResultSet* result = pstmt->executeQuery();
    while (result->next()) {
        tmpVector.push_back(Map(result->getInt("MapId"), result->getInt("ZoneId"), result->getString("Name"), result->getDouble("XRange"), result->getDouble("YRange")));
    }

    delete pstmt;

    return tmpVector.back();
}

void Database::getAllZones()
{
    _zones.clear();

    sql::Statement* stmt = _connection->createStatement();
    sql::ResultSet* result = stmt->executeQuery("select * from zone");
    while (result->next()) {
        _zones.push_back(Zone(result->getInt("ZoneId"), result->getString("Name"), result->getString("Description")));
    }

    delete stmt;
    delete result;
}

void Database::getAllMaps()
{
    _maps.clear();

    sql::Statement* stmt = _connection->createStatement();
    sql::ResultSet* result = stmt->executeQuery("select * from map");
    while (result->next()) {
        _maps.push_back(Map(result->getInt("MapId"), result->getInt("ZoneId"), result->getString("Name"), result->getDouble("XRange"), result->getDouble("YRange")));
    }

    delete stmt;
    delete result;
}

void Database::getAllTracks()
{
    _tracks.clear();

    sql::Statement* stmt = _connection->createStatement();
    sql::ResultSet* result = stmt->executeQuery("select TrackId, MapId, TrackName from track");
    while (result->next()) {
        _tracks.push_back(Track(result->getInt("TrackId"), result->getInt("MapId"), result->getString("TrackName")));
    }

    delete stmt;
    delete result;
}

void Database::getAllTrackMarks()
{
    _trackMarks.clear();

    sql::Statement* stmt = _connection->createStatement();
    sql::ResultSet* result = stmt->executeQuery("select * from trackmark");
    while (result->next()) {
        _trackMarks.push_back(TrackMark(result->getInt("TrackMarkId"), result->getInt("TrackId"), result->getString("MarkName"), result->getString("MarkDescription"), result->getBoolean("IsActive")));
    }

    delete stmt;
    delete result;
}
