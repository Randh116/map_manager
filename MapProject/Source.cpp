#include <stdlib.h>
#include <iostream>

#include "mysql_connection.h"
#include <cppconn/driver.h>
#include <cppconn/exception.h>
#include <cppconn/prepared_statement.h>

#include "Database.h"

using namespace std;

//const std::string server = "tcp://127.0.0.1:3306";
//const std::string username = "root";
//const std::string password = "P4r0owK@";
//const std::string schema = "mapdb";

std::string server;
std::string username;
std::string password;
std::string schema;

int main()
{
    cout << "Przyklad skryptu wykonujacego odczyt i zapis informacji w polaczeniu z baza danych." << std::endl;
    cout << "W celu wpisania domyslnych informacji wcisnij enter zostawiajac pusta wartosc." << std::endl;
    cout << "Podaj lokalny adres servera MySQL (domyslnie tcp://127.0.0.1:3306): ";
    getline(cin, server);
    //cin >> server;
    if (server.empty()) {
        server = "tcp://127.0.0.1:3306";
    }
    cout << "Wprowadzona wartosc: " << server << std::endl;
    cout << "Podaj nazwe uzytkownika (domyslnie root): ";
    getline(cin, username);
    //cin >> username;
    if (username.empty()) {
        username = "root";
    }
    cout << "Wprowadzona wartosc: " << username << std::endl;
    cout << "Podaj haslo do servera (domyslnie root): ";
    getline(cin, password);
    //cin >> password;
    if (password.empty()) {
        password = "root";
    }
    cout << "Podaj nazwe bazy danych (domyslnie mapdb): ";
    getline(cin, schema);
    //cin >> schema;
    if (schema.empty()) {
        schema = "mapdb";
    }
    cout << "Wprowadzona wartosc: " << schema << std::endl;

    Database* db = new Database(server, username, password, schema);
    sql::Connection* con = db->connect();

    cout << "Polaczono z baza danych. Nastapi zalozenie tabel." << std::endl;

    sql::Statement* stmt;

    stmt = con->createStatement();
    stmt->execute("DROP TABLE IF EXISTS zone");
    cout << "Finished dropping table Zone (if existed)" << endl;
    stmt->execute("CREATE TABLE zone (ZoneId INTEGER PRIMARY KEY, Name VARCHAR(45), Description VARCHAR(45));");
    cout << "Finished creating table Zone" << endl;

    stmt = con->createStatement();
    stmt->execute("DROP TABLE IF EXISTS map");
    cout << "Finished dropping table Map (if existed)" << endl;
    stmt->execute("CREATE TABLE map (MapId INTEGER PRIMARY KEY, ZoneId INTEGER, Name VARCHAR(45), XRange DECIMAL(4,2), YRange DECIMAL(4,2));");
    cout << "Finished creating table Map" << endl;

    stmt = con->createStatement();
    stmt->execute("DROP TABLE IF EXISTS track");
    cout << "Finished dropping table Track (if existed)" << endl;
    stmt->execute("CREATE TABLE track (TrackId INTEGER PRIMARY KEY, MapId INTEGER, TrackName VARCHAR(45));");
    cout << "Finished creating table Track" << endl;

    stmt = con->createStatement();
    stmt->execute("DROP TABLE IF EXISTS trackmark");
    cout << "Finished dropping table TrackMark (if existed)" << endl;
    stmt->execute("CREATE TABLE trackmark (TrackMarkId INTEGER PRIMARY KEY, TrackId INTEGER, MarkName VARCHAR(45), MarkDescription VARCHAR(45), IsActive TINYINT(1));");
    cout << "Finished creating table TrackMark" << endl;

    delete stmt;

    cout << "Nastapi dodanie dwoch obiektow klasy Zone do bazy danych, a nastepnie wyswietlenie ich parametrow." << std::endl;

    Zone* zone1 = new Zone(con, 1, "strefa pierwsza", "opis strefy pierwszej");
    Zone* zone2 = new Zone(con, 2, "strefa druga", "opis strefy drugiej");

    db->addToDb(zone1);
    db->addToDb(zone2);
    db->getAllZones();
    db->displayZones();

    cout << "Nastapi dodanie dwoch obiektow klasy Map do bazy danych, a nastepnie wyswietlenie ich parametrow." << std::endl;

    Map* map1 = new Map(con, 1, 1, "mapa pierwsza", 1.2, 3.4);
    Map* map2 = new Map(con, 2, 2, "mapa druga", 2.3, 4.5);

    db->addToDb(map1);
    db->addToDb(map2);
    db->getAllMaps();
    db->displayMaps();

    cout << "Nastapi dodanie czterech obiektow klasy Track do bazy danych, a nastepnie wyswietlenie ich parametrow." << std::endl;

    Track* track1 = new Track(con, 1, 1, "trasa pierwsza");
    Track* track2 = new Track(con, 2, 1, "trasa druga");
    Track* track3 = new Track(con, 3, 2, "trasa trzecia");
    Track* track4 = new Track(con, 4, 2, "trasa czwarta");

    db->addToDb(track1);
    db->addToDb(track2);
    db->addToDb(track3);
    db->addToDb(track4);
    db->getAllTracks();
    db->displayTracks();

    cout << "Nastapi dodanie osmiu obiektow klasy TrackMark do bazy danych, a nastepnie wyswietlenie ich parametrow." << std::endl;

    TrackMark* tm1 = new TrackMark(con, 1, 1, "znacznik pierwszy", "opis pierwszy", true);
    TrackMark* tm2 = new TrackMark(con, 2, 2, "znacznik drugi", "opis drugi", true);
    TrackMark* tm3 = new TrackMark(con, 3, 3, "znacznik trzeci", "opis trzeci", true);
    TrackMark* tm4 = new TrackMark(con, 4, 4, "znacznik czwarty", "opis czwarty", true);
    TrackMark* tm5 = new TrackMark(con, 5, 1, "znacznik piaty", "opis piaty", true);
    TrackMark* tm6 = new TrackMark(con, 6, 2, "znacznik szosty", "opis szosty", true);
    TrackMark* tm7 = new TrackMark(con, 7, 3, "znacznik siodmy", "opis siodmy", true);
    TrackMark* tm8 = new TrackMark(con, 8, 4, "znacznik osmy", "opis osmy", true);

    db->addToDb(tm1);
    db->addToDb(tm2);
    db->addToDb(tm3);
    db->addToDb(tm4);
    db->addToDb(tm5);
    db->addToDb(tm6);
    db->addToDb(tm7);
    db->addToDb(tm8);
    db->getAllTrackMarks();
    db->displayTrackMarks();

    cout << "Nastapi pobranie obiektu klasy Zone z bazy za pomoca numeru id wraz z wysw. parametrow." << std::endl;

    Zone exampleZoneById = db->getZoneById(1);
    cout << exampleZoneById.show() << std::endl;

    cout << "Nastapi pobranie obiektu klasy Map z bazy za pomoca numeru id wraz z wysw. parametrow." << std::endl;

    Map exampleMapById = db->getMapById(1);
    cout << exampleMapById.show() << std::endl;

    cout << "Nastapi pobranie obiektu klasy Zone z bazy za pomoca nazwy strefy wraz z wysw. parametrow." << std::endl;

    Zone exampleZoneByName = db->getZoneByName("strefa druga");
    cout << exampleZoneByName.show() << std::endl;

    cout << "Nastapi pobranie obiektu klasy Map z bazy za pomoca nazwy mapy wraz z wysw. parametrow." << std::endl;

    Map exampleMapByName = db->getMapByName("mapa druga");
    cout << exampleMapByName.show() << std::endl;

    cout << "Nastapi pobranie obiektu klasy Track z bazy za pomoca numeru id wraz z wysw. parametrow." << std::endl;

    Track exampleTrack = db->getTrackById(1);
    cout << exampleTrack.show() << std::endl;

    cout << "Nastapi pobranie obiektu klasy TrackMark z bazy za pomoca numeru id wraz z wysw. parametrow." << std::endl;

    TrackMark exampleTrackMark = db->getTrackMarkById(1);
    cout << exampleTrackMark.show() << std::endl;

    cout << "Zwalnianie pamieci." << std::endl;

    delete zone1, zone2;
    delete map1, map2;
    delete track1, track2, track3, track4;
    delete tm1, tm2, tm3, tm4, tm5, tm6, tm7, tm8;
    delete db;
    delete con;
    system("pause");
    return 0;
}