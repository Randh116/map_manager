#include "Map.h"

Map::Map(sql::Connection* conn, int mapId, int zoneId, std::string name, double xRange, double yRange)
{
	_connection = conn;
	_mapId = mapId;
	_zoneId = zoneId;
	_name = name;
	_xRange = xRange;
	_yRange = yRange;
}

Map::Map(int mapId, int zoneId, std::string name, double xRange, double yRange)
{
	//_connection = nullptr;
	_mapId = mapId;
	_zoneId = zoneId;
	_name = name;
	_xRange = xRange;
	_yRange = yRange;
}

std::vector<Track> Map::getAssignedTracks()
{
	return std::vector<Track>();
}

std::string Map::show() const
{
	return "Map -> id: " + std::to_string(_mapId) + "; zoneId: " + std::to_string(_zoneId) + "; name: " + _name + "; xRange: " + std::to_string(_xRange) + "; yRange: " + std::to_string(_yRange);
}
