#pragma once
#include <string>
#include <vector>
#include <mysql_connection.h>
#include "TrackMark.h"

class Track
{
public:
	Track(sql::Connection* conn,
		int trackId,
		int mapId,
		std::string name);

	Track(int trackId,
		int mapId,
		std::string name);

	std::vector<TrackMark> getAssignedTrackMarks();

	int getTrackId() { return _trackId; }
	int getMapId() { return _mapId; }
	std::string getName() { return _name; }
	
	std::string show() const;

private:
	int _trackId;
	int _mapId;
	std::string _name;

	sql::Connection* _connection;
	std::vector<TrackMark> _trackMarks;
};

