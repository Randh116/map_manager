#include "TrackMark.h"

TrackMark::TrackMark(sql::Connection* conn, int trackMarkId, int trackId, std::string name, std::string description, bool isActive)
{
	_connection = conn;
	_trackMarkId = trackMarkId;
	_trackId = trackId;
	_name = name;
	_description = description;
	_isActive = isActive;
}

TrackMark::TrackMark(int trackMarkId, int trackId, std::string name, std::string description, bool isActive)
{
	//_connection = nullptr;
	_trackMarkId = trackMarkId;
	_trackId = trackId;
	_name = name;
	_description = description;
	_isActive = isActive;
}

std::string TrackMark::show() const
{
	return "TrackMark -> id: " + std::to_string(_trackMarkId) + "; trackId: " + std::to_string(_trackId) + "; name: " + _name + "; description: " + _description + "; active: " + std::to_string(_isActive);
}
