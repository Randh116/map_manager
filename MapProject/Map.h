#pragma once
#include <string>
#include <vector>
#include "Track.h"

class Map
{
public:
	Map(sql::Connection* conn,
		int mapId,
		int zoneId,
		std::string name,
		double xRange,
		double yRange);

	Map(int mapId,
		int zoneId,
		std::string name,
		double xRange,
		double yRange);

	std::vector<Track> getAssignedTracks();

	std::string show() const;

	int getMapId() { return _mapId; }
	int getZoneId() { return _zoneId; }
	std::string getName() { return _name; }
	double getXRange() { return _xRange; }
	double getYRange() { return _yRange; }

private:
	int _mapId;
	int _zoneId;
	std::string _name;
	double _xRange;
	double _yRange;

	sql::Connection* _connection;
	std::vector<Track> _tracks;
};

