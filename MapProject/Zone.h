#pragma once
#include <string>
#include <vector>
#include <mysql_connection.h>
#include "Track.h"
#include "Map.h"

class Zone
{
public:
	Zone(sql::Connection* conn,
		int zoneId,
		std::string name,
		std::string description);

	Zone(int zoneId,
		std::string name,
		std::string description);

	std::vector<Map> getAssignedMaps();

	int getZoneId() { return _zoneId; }
	std::string getName() { return _name; }
	std::string getDescription() { return _description; }

	std::string show() const;

private:
	int _zoneId;
	std::string _name;
	std::string _description;

	sql::Connection* _connection;
	std::vector<Map> _maps;
};

