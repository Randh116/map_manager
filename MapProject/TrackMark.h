#pragma once
#include <string>
#include <mysql_connection.h>

class TrackMark
{
public:
	TrackMark(sql::Connection* conn,
		int trackMarkId,
		int trackId,
		std::string name,
		std::string description,
		bool isActive);

	TrackMark(int trackMarkId,
		int trackId,
		std::string name,
		std::string description,
		bool isActive);

	std::string show() const;

	int getTrackMarkId() { return _trackMarkId; }
	int getTrackId() { return _trackId; }
	std::string getName() { return _name; }
	std::string getDescription() { return _description; }
	bool getActive() { return _isActive; }

private:
	int _trackMarkId;
	int _trackId;
	std::string _name;
	std::string _description;
	bool _isActive;

	sql::Connection* _connection;
};

