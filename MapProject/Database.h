#pragma once
#include <vector>
#include "Map.h"
#include "Track.h"
#include "TrackMark.h"
#include "Zone.h"

class Database
{
public:
	Database(std::string server, std::string username, std::string password, std::string schema);
	sql::Connection* connect();

	void displayMaps() const;
	void displayTracks() const;
	void displayTrackMarks() const;
	void displayZones() const;

	bool addToDb(Zone* zone);
	bool addToDb(Map* map);
	bool addToDb(Track* track);
	bool addToDb(TrackMark* trackMark);

	bool deleteFromDb(Zone* zone);
	bool deleteFromDb(Map* map);
	bool deleteFromDb(Track* track);
	bool deleteFromDb(TrackMark* trackMark);

	Zone getZoneById(int id);
	Map getMapById(int id);
	Track getTrackById(int id);
	TrackMark getTrackMarkById(int id);

	Zone getZoneByName(std::string name);
	Map getMapByName(std::string name);

	void getAllZones();
	void getAllMaps();
	void getAllTracks();
	void getAllTrackMarks();

private:
	bool execAdd(sql::PreparedStatement* pstmt);
	bool execDelete(sql::PreparedStatement* pstmt);

	sql::Connection* _connection;

	std::vector<Zone> _zones;
	std::vector<Map> _maps;
	std::vector<Track> _tracks;
	std::vector<TrackMark> _trackMarks;

	std::string _server;
	std::string _username;
	std::string _password;
	std::string _schema;
};

