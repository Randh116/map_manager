#include "Track.h"

Track::Track(sql::Connection* conn, int trackId, int mapId, std::string name)
{
	_connection = conn;
	_trackId = trackId;
	_mapId = mapId;
	_name = name;
}

Track::Track(int trackId, int mapId, std::string name)
{
	//_connection = nullptr;
	_trackId = trackId;
	_mapId = mapId;
	_name = name;
}

std::vector<TrackMark> Track::getAssignedTrackMarks()
{
	return std::vector<TrackMark>();
}

std::string Track::show() const
{
	return "Track -> id: " + std::to_string(_trackId) + "; mapId: " + std::to_string(_mapId) + "; name: " + _name;
}